CREATE SCHEMA `addressbook`;

CREATE TABLE `addressbook`.`addressbook` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL, 
  PRIMARY KEY (`id`));
  -- add city state zip