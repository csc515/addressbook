package edu.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import edu.missouristate.domain.Address;
import edu.missouristate.service.AddressbookService;

@Controller
public class AddressbookController {

	@Autowired
	AddressbookService addressbookService;
	
	@GetMapping(value="/")
	public String getIndex(Model model) {
		List<Address> addressList = addressbookService.getAddressList();
		model.addAttribute("addressList", addressList);
		return "index";
	}
	
	@GetMapping(value="/addAddress")
	public String getAddAddress(Model model) {
		return "addAddress";
	}
	
	@PostMapping(value="/addAddress")
	public String postAddAddress(Model model, @RequestBody Address address) {
		// call the service..  addressbookService.saveAddress(address)
		return "redirect:/";
	}
	
	@GetMapping(value="/sw.js")
	public String getSw(Model model) {
		return "";
	}
}
