package edu.missouristate.repository;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.domain.Address;

public interface AddressbookRepository extends CrudRepository<Address, Integer>, AddressbookRepositoryCustom {
	// Spring Data abstract methods go here
	
}
