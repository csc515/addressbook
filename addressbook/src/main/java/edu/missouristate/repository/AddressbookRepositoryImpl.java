package edu.missouristate.repository;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.Address;

@Repository
public class AddressbookRepositoryImpl extends QuerydslRepositorySupport implements AddressbookRepositoryCustom {
	
	public AddressbookRepositoryImpl() {
		super(Address.class);
	}

	// Object Oriented SQL goes here...
	
	
}
