package edu.missouristate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Address;
import edu.missouristate.repository.AddressbookRepository;

@Service("addressbookService")
public class AddressbookService {

	@Autowired
	AddressbookRepository addressbookRepo;

	public List<Address> getAddressList() {
		return (List<Address>) addressbookRepo.findAll();
	}
	
	@Transactional
	public void saveAddress(Address address) {
		addressbookRepo.save(address);
	}
	
	@Transactional
	public void updateAddress(Address address) {
		addressbookRepo.save(address);
	}
	
	public Address getAddressById(Integer id) {
		return addressbookRepo.findById(id).get();
	}
	
	
}
